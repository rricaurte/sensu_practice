# Training Project

## 1st part: Sensu Server

> * Install and hypervisor for virtual machines like VirtualBox, VMWare or similar.
> * Create a CentOS 7 virtual machine and install Sensu monitoring system and
>   use Uchiwa as an alert dashboard.
> * Configure Sensu to start monitoring basic O.S. metrics of the virtual machine.

- Virtual Environment: VirtualBox.
- Provisioning tool: vagrant.
- Guest OS: ubuntu/bionic64.

In order to automate the installation and configuration processes, I drafted an
Ansible’s Playbook: sensu\_config.yml.
